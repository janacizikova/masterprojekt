package com.example.masterukol1.presenter;

import android.content.Context;
import android.widget.Toast;

import com.example.masterukol1.model.Note;
import com.orm.SugarApp;

import java.util.ArrayList;
import java.util.List;

public class ServiceDatabase{

    List <Note> noteList;
    Context context;

    public ServiceDatabase() {
    }

    public ServiceDatabase(Context context) {
        this.context = context;
    }

    public void saveNote(Note newNote) {
        newNote.save();
    }

    public List<Note> listAll(){
        return Note.listAll(Note.class);
    }

//    public Note getNote(int id) {
//        return Note.findById(Note.class, id);
//    }

    public Note getNote(int positionInList) {
        noteList = this.listAll();
            return noteList.get(positionInList);
    }

    public void deleteNote() {

        noteList = this.listAll();
        Boolean nothingToDelete = true;

        for (Note note : noteList) {
            if (note.getDone()) {
                note.delete();
                nothingToDelete = false;
            }
        }
        if(nothingToDelete) {
            Toast.makeText(this.context, "Žádný soubor ke smazání", Toast.LENGTH_LONG).show();
        }
    };
}
