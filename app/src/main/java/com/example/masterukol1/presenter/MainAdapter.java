package com.example.masterukol1.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;


import com.example.masterukol1.R;
import com.example.masterukol1.model.Note;

/**
 * Created by Jana on 02/11/2017.
 */

public class MainAdapter extends BaseAdapter {

    private Context mainContext;
    private ServiceDatabase serviceDatabase;


    //konstruktor
    public MainAdapter(Context context) {
        this.mainContext = context;
        this.serviceDatabase = new ServiceDatabase();
    }

    //vrati pocet prvku seznamu
    @Override
    public int getCount() {
        return serviceDatabase.listAll().size();
    }

    //vrati jednu poznamku ze seznamu, ktery obsahuje obsah sugar db
    @Override
    public Note getItem(int position) {
        return serviceDatabase.getNote(position);
    }


    //vrati id poznamky ze seznamu obsahu sugar DB podle id
    @Override
    public long getItemId(int position) {
        return serviceDatabase.getNote(position).getId();
    }


    //vrati nam jeden radek z ListView (prvek layoutu) naplneny hodnotami
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        System.out.println("position jest" + position);

        //najdu si layout, kde mam vizual pro jeden radek listView
        View result = LayoutInflater.from(mainContext).inflate(R.layout.activity_main_list_detail, viewGroup, false); //toto je result, co nam to vrati

        //z objektu poznamky ze seznamu si vyplnim udaje do jednoho prvku listview

        CheckBox checkBox = result.findViewById(R.id.done_check_box);
        TextView titleTxt = result.findViewById(R.id.title_list_text);
        TextView descriptionTxt = result.findViewById(R.id.description_list_text);


// final je to kvůli použití setOnCheckedChangeListener na checkboxu (ten to vyžaduje)
        final Note note = getItem(position);


        if (note == null) {
            return result;
        }

        titleTxt.setText(note.getTitle());
        descriptionTxt.setText(note.getDescription());
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                note.setDone(isChecked);
                serviceDatabase.saveNote(note);
            }
        });

//        na prvek checkbox v layoutu si nastavím hodnotu z objektu Note (
        checkBox.setChecked(note.getDone());
//        tady si vrátím jeden řádek s upravenýmmi daty
        return result;
    }


}


