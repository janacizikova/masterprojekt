package com.example.masterukol1.presenter;

import android.content.Context;
import android.widget.ListView;
import android.widget.Toast;

import com.example.masterukol1.model.Note;
import com.example.masterukol1.presenter.MainAdapter;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class OpenCSVWriter {

    public static final String contentPath = new String (android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + "/MasterUkolBackup.csv");


    public void exportNoteList() throws IOException
    {

        System.out.println("proměnná csv " + contentPath );

        CSVWriter csvWriter = new CSVWriter(new FileWriter(contentPath));



        List<String[]> noteStringList = new ArrayList<>();
        List<Note> noteList = Note.listAll(Note.class);

        for(Note note : noteList){
            noteStringList.add(note.toArrayString());
        }

// pro novější androidy bych mohla použít Writer writer = Files.newBufferedWriter(Paths.get(STRING_ARRAY_SAMPLE));
// https://www.callicoder.com/java-read-write-csv-file-opencsv/

        csvWriter.writeAll(noteStringList);

        csvWriter.close();

    }


    public void importNoteList(ListView listView, MainAdapter mainAdapter, Context context) throws IOException{


        File file = new File(contentPath);

        if(file.length() == 0) {
            Toast.makeText(context, "Žádný soubor k importu", Toast.LENGTH_LONG).show();
        }

        System.out.println("mam prazdny souhbor a pokracuje mi to dal");

        file.createNewFile();
        Reader reader = new FileReader(file);
        System.out.println("reader " + reader);
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> noteStringList = new ArrayList<>();



        String[] line;
        while ((line = csvReader.readNext()) != null) {
            noteStringList.add(line);
            Note note = new Note(line[0], line[1], Boolean.parseBoolean(line[2]));
            System.out.println("note " + line[0]);
            note.save();

        }


        mainAdapter.notifyDataSetChanged();

        reader.close();
        csvReader.close();

    }



    public List<String[]> oneByOne(Reader reader) throws Exception {
        List<String[]> list = new ArrayList<>();
        CSVReader csvReader = new CSVReader(reader);
        String[] line;
        while ((line = csvReader.readNext()) != null) {
            list.add(line);
        }
        reader.close();
        csvReader.close();
        return list;
    }

}
