package com.example.masterukol1.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.widget.TextView;

import com.example.masterukol1.R;
import com.example.masterukol1.model.Note;
import com.example.masterukol1.presenter.ServiceDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityDisplayNote extends AppCompatActivity {

    @BindView(R.id.title_text) TextView titleText;
    @BindView(R.id.description_text) TextView descriptionText;

    private ServiceDatabase serviceDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_note);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        int noteIndex = intent.getIntExtra("note_index", -1);

        serviceDatabase = new ServiceDatabase();

        Note note = serviceDatabase.getNote(noteIndex);

        titleText.setText(note.getTitle());
        descriptionText.setText(note.getDescription());


    }
}
