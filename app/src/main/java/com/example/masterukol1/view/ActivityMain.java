package com.example.masterukol1.view;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.masterukol1.presenter.MainAdapter;
import com.example.masterukol1.presenter.OpenCSVWriter;
import com.example.masterukol1.R;
import com.example.masterukol1.presenter.ServiceDatabase;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityMain extends AppCompatActivity {

    @BindView(R.id.main_list) ListView mainList;
    @BindView(R.id.add_btn) Button addBtn;
    @BindView(R.id.delete_btn) Button deleteBtn;

    private MainAdapter mainAdapter;
    private OpenCSVWriter openCSVWriter;
    private Activity activity;
    private ServiceDatabase serviceDatabase;


//    nastevní overflow menu - získání menu layoutu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Get menu inflater object.
        MenuInflater menuInflater = getMenuInflater();

        // Inflate the custom overflow menu
        menuInflater.inflate(R.menu.overflow_menu, menu);

        return true;
    }

//  nastevní overflow menu - spuštění akce při kliknutí na prvek menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //získání layoutu, kde má být overflow menu
        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.activity_main_layout);

        // získání id kliknuté položky v menu
        int itemId = item.getItemId();
        if(itemId == R.id.export_note_list)
        {
//            mainLayout.setBackgroundColor(Color.RED);
            OpenCSVWriter openCSVWriter = new OpenCSVWriter();
            try {
                openCSVWriter.exportNoteList();
            } catch (IOException ex) {
                System.out.println("IOException se vyskytla");
            }


        }else if(itemId == R.id.import_note_list)
        {
//            mainLayout.setBackgroundColor(Color.YELLOW);
            try {

                openCSVWriter.importNoteList(mainList, mainAdapter, activity);
            } catch (IOException ex) {
                System.out.println("IOException se vyskytla");
            }

        }

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


//        nastevní listView

        mainAdapter = new MainAdapter(this);
        mainList.setAdapter(mainAdapter);
        activity = this;
        serviceDatabase = new ServiceDatabase(this);

//        service pro import/export

        openCSVWriter = new OpenCSVWriter();

//        nastavení btn


        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMain.this, ActivityAddNote.class));
            }
        });



        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceDatabase.deleteNote();
                mainAdapter.notifyDataSetChanged();
            }
        });

//        akce při kliknutí na položku ListView

        mainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                displayNote(position);
            }
        });
    }

    public void displayNote(int notePosition) {
        Intent intent = new Intent(ActivityMain.this, ActivityDisplayNote.class);
        intent.putExtra("note_index", notePosition);
        startActivity(intent);
    }



//TODO pridat do Note id a delat hledani podle id


}
