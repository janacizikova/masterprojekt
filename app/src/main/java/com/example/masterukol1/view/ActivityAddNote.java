package com.example.masterukol1.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.masterukol1.R;
import com.example.masterukol1.presenter.ServiceDatabase;
import com.example.masterukol1.model.Note;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityAddNote extends AppCompatActivity {

    @BindView(R.id.title_edit) EditText titleEdit;
    @BindView(R.id.description_edit) EditText descriptionEdit;
    @BindView(R.id.save_btn) Button saveBtn;

    ServiceDatabase serviceDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        ButterKnife.bind(this);

        serviceDatabase = new ServiceDatabase();

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(titleEdit.getText())){
                    System.out.println("Nevyplnili jste pole");
                    titleEdit.setError("Nevyplnili jste pole");
                    return;
                }

                System.out.println("title " + titleEdit.getText().toString() + "description " + descriptionEdit.getText().toString());

                Note newNote = new Note(titleEdit.getText().toString(),descriptionEdit.getText().toString());
                serviceDatabase.saveNote(newNote);
                finish();
            }
        });




    }



}
