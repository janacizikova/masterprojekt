package com.example.masterukol1.model;

import com.orm.SugarRecord;

public class Note extends SugarRecord {

    private String title;
    private String description;
    private Boolean isDone;

    public Note() {
    }

    public Note(String title, String description) {
        this.title = title;
        this.description = description;
        this.isDone = false;
    }


    public Note(String title, String description, Boolean isDone) {
        this.title = title;
        this.description = description;
        this.isDone = isDone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getDone() {
        return isDone;
    }

    public void setDone(Boolean done) {
        isDone = done;
    }


    public String[] toArrayString() {
        return new String[] {this.title, this.description, this.isDone.toString()};
    }
}
